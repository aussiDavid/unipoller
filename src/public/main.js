$(function() {
  var answered = false;

  var Counter = (function (options) {
    var _running = false;
    var _interval = 1000;
    var _selector;
    var _intervalEvent;
    var _startValue = 10;
    var _endValue = 0;
    var _step = -1;
    var _timeoutEvent = function() {};

    var _counterEvent;

    var running = function () {
      return _running;
    };

    var start = function (startValue) {
      if (_running)
        return;

      if(startValue)
        _startValue = startValue;

      show();
      $(_selector).text(_startValue);
      var intervalFunction = function() {
        var counterValue = parseInt($(_selector).text());
        
        if(counterValue < _endValue-_step) {
          _timeoutEvent();
          stop();
          return;
        }
        
        counterValue += _step;

        $(_selector).text(counterValue);
      };

      _counterEvent = setInterval(intervalFunction, _interval);
      _running = true;
      // intervalFunction();
    };
    
    var stop = function () {
      if (!_running)
        return;

      clearTimeout(_counterEvent);
      _running = false;
    };

    var setCounterEvent = function(counterFunction) {
      if (!counterFunction || typeof(counterFunction) !== "function" || _running)
        return;

      _function = counterFunction;
    }

    var setCounterSelecter = function(selector) {
      if (!selector || _running)
        return;

      _selector = selector; 
    }

    var setCounterText = function(text) {
      if(!text)
        return;

      $(_selector).text(text);
    }

    var enable = function() {
      $(_selector).prop("disabled", false);
    }

    var disable = function() {
      $(_selector).prop("disabled", true);
    }

    var show = function() {
      $(_selector).show();
    }

    var hide = function() {
      $(_selector).hide();
    }

    if(options){
      if(options.selector) {
        _selector = options.selector;
      }

      if(options.text && options.selector) {
        $(_selector).text(options.text);
      }

      if(options.timeoutEvent && typeof(options.timeoutEvent) === "function"){
        _timeoutEvent = options.timeoutEvent;
      }

      if(options.startValue) {
        _startValue = options.startValue;
      }

      if(options.endValue) {
        _endValue = options.endValue;
      }
      
      if(options.step) {
        _step = options.step;
      }
    }
    
    return {
      enable: enable,
      disable: disable,
      setCounterText: setCounterText,
      setCounterEvent: setCounterEvent,
      setCounterSelecter: setCounterSelecter,
      stop: stop,
      start: start,
      show: show,
      hide: hide,
      running: running
    };

  });

  Chart.defaults.global = {
      // Boolean - Whether to animate the chart
      animation: true,

      // Number - Number of animation steps
      animationSteps: 60,

      // String - Animation easing effect
      // Possible effects are:
      // [easeInOutQuart, linear, easeOutBounce, easeInBack, easeInOutQuad,
      //  easeOutQuart, easeOutQuad, easeInOutBounce, easeOutSine, easeInOutCubic,
      //  easeInExpo, easeInOutBack, easeInCirc, easeInOutElastic, easeOutBack,
      //  easeInQuad, easeInOutExpo, easeInQuart, easeOutQuint, easeInOutCirc,
      //  easeInSine, easeOutExpo, easeOutCirc, easeOutCubic, easeInQuint,
      //  easeInElastic, easeInOutSine, easeInOutQuint, easeInBounce,
      //  easeOutElastic, easeInCubic]
      animationEasing: "easeOutQuart",

      // Boolean - If we should show the scale at all
      showScale: true,

      // Boolean - If we want to override with a hard coded scale
      scaleOverride: false,

      // ** Required if scaleOverride is true **
      // Number - The number of steps in a hard coded scale
      scaleSteps: null,
      // Number - The value jump in the hard coded scale
      scaleStepWidth: null,
      // Number - The scale starting value
      scaleStartValue: null,

      // String - Colour of the scale line
      scaleLineColor: "rgba(0,0,0,.1)",

      // Number - Pixel width of the scale line
      scaleLineWidth: 1,

      // Boolean - Whether to show labels on the scale
      scaleShowLabels: true,

      // Interpolated JS string - can access value
      scaleLabel: "<%=value%>",

      // Boolean - Whether the scale should stick to integers, not floats even if drawing space is there
      scaleIntegersOnly: true,

      // Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: false,

      // String - Scale label font declaration for the scale label
      scaleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

      // Number - Scale label font size in pixels
      scaleFontSize: 16,

      // String - Scale label font weight style
      scaleFontStyle: "bold",

      // String - Scale label font colour
      scaleFontColor: "white",

      // Boolean - whether or not the chart should be responsive and resize when the browser does.
      responsive: false,

      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: false,

      // Boolean - Determines whether to draw tooltips on the canvas or not
      showTooltips: false,

      // Function - Determines whether to execute the customTooltips function instead of drawing the built in tooltips (See [Advanced - External Tooltips](#advanced-usage-custom-tooltips))
      customTooltips: false,

      // Array - Array of string names to attach tooltip events
      tooltipEvents: ["mousemove", "touchstart", "touchmove"],

      // String - Tooltip background colour
      tooltipFillColor: "rgba(0,0,0,0.8)",

      // String - Tooltip label font declaration for the scale label
      tooltipFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

      // Number - Tooltip label font size in pixels
      tooltipFontSize: 14,

      // String - Tooltip font weight style
      tooltipFontStyle: "normal",

      // String - Tooltip label font colour
      tooltipFontColor: "#fff",

      // String - Tooltip title font declaration for the scale label
      tooltipTitleFontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",

      // Number - Tooltip title font size in pixels
      tooltipTitleFontSize: 14,

      // String - Tooltip title font weight style
      tooltipTitleFontStyle: "bold",

      // String - Tooltip title font colour
      tooltipTitleFontColor: "#fff",

      // Number - pixel width of padding around tooltip text
      tooltipYPadding: 6,

      // Number - pixel width of padding around tooltip text
      tooltipXPadding: 6,

      // Number - Size of the caret on the tooltip
      tooltipCaretSize: 8,

      // Number - Pixel radius of the tooltip border
      tooltipCornerRadius: 6,

      // Number - Pixel offset from point x to tooltip edge
      tooltipXOffset: 10,

      // String - Template string for single tooltips
      tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %>",

      // String - Template string for multiple tooltips
      multiTooltipTemplate: "<%= value %>",

      // Function - Will fire on animation progression.
      onAnimationProgress: function(){},

      // Function - Will fire on animation completion.
      onAnimationComplete: function(){}
  }

  var url = window.location.pathname;
  // var filename = url.substring(url.lastIndexOf('/')+1);
  // var namespace = 'http://'+window.location.host+'/'+filename;
  // var socket = io.connect(namespace);
  var socket = io.connect();
  var waitingForConnectionTimer = setTimeout(function() {
      $(".title").fadeTo("You wern't able to connect to the server.<br>Are you sure you are in the right place?");
    $(".subtitle").fadeTo("");
    socket.close();
  }, 3000);

  var counter = new Counter({
    selector: ".counter",
    startValue: 5,
    endValue: 0,
    step: -1,
    timeoutEvent: function () {
      $(".option").addClass("notpicked");
      $(".option").removeClass("option");
      // socket.emit('submit answer', {timedout: true});
      answered = true;
    }
  });
 
  // Socket events
  socket.on('waiting for users', function (users) {
    $(".title").fadeTo("Waiting for "+(users ? users : "") +" others to join<span>.</span><span>.</span><span>.</span>");
    $(".subtitle").fadeTo("");
  });

  socket.on('quiz in-progress', function () {
    $(".title").fadeTo("Quiz is in progress");
    $(".subtitle").fadeTo("");
  });

  socket.on('ping', function () {
      socket.emit('ping');
      clearTimeout(waitingForConnectionTimer);
  });

  socket.on('quiz', function (quiz) {
    answered = false;
    counter.stop();
    if(quiz.timeout) {
      counter.start(quiz.timeout);
      counter.show();
    }
    else {
      counter.hide();
    } 

    var choices = quiz.choices.map(function(choice) {
      return "<a class=\"option\" href=\"javascript:void(0)\">"+choice+"</a>"; 
    });
    
    choices = choices.join(' ');

    $(".title").fadeTo(quiz.question);
    $(".subtitle").fadeTo(choices, function() {
      $(".option").click(function() {
        if (answered) 
          return;
        
        socket.emit('submit answer', {
          selection: $(this).text(), 
          quizId:quiz.id
        });     

        $(this).removeClass("option");
        $(this).addClass("picked");
        $(this).siblings().addClass("notpicked");
        
        counter.stop();
        counter.disable();
        counter.setCounterText('Waiting for others');
        counter.show();

        answered = true;
      });
    });

    $("#canvas").html("");
    
    
  });

  socket.on('finish', function (finish) {
    counter.stop();
    counter.hide();
    $(".title").fadeTo(finish.message);
    $(".subtitle").fadeTo("");
    if(Math.min.apply(null,finish.data.datasets[0].data) <= 0)
      return;

    $("#canvas").html("<canvas id=\"myChart\" width=\"600\" height=\"500\"></canvas>");
    var ctx = document.getElementById("myChart").getContext("2d");
    window.myBar = new Chart(ctx).Bar(finish.data, finish.options);
  });
});

$.fn.fadeTo = function (text, cb) {
    this.fadeOut(function() {
      $(this).html(text).fadeIn();
      if(cb)
        cb()
    });
  }
