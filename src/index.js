var Server = require('../lib/server.js');
var Quiz = require('./quiz.js');

var server = Server.createServer(3000);

/**
* Constants
**/

var buildQuiz = server.io.of('/build_quiz.html');

/**
 * Socket connection that handels the build quiz page
 **/
buildQuiz.on('connection', function (socket) {
	console.log("user connected to build quiz");
	
	socket.on('new quiz', function(quizToBuild){
		console.log('new question recieved');
		var newQuiz = Quiz.createQuiz(quizToBuild);
		socket.emit('new quiz link',newQuiz.server.port);

		socket.on('start quiz', function() {
			newQuiz.startQuiz();
		})
	});
});