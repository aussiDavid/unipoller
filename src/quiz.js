
/**
 * Module dependencies.
 */

var Server = require('../lib/server.js');
var TimerJob = require('timer-jobs');
var EventEmitter = require("events").EventEmitter;
var Validate = require("validate.js");
require('array.prototype.find');


/**
 * Expose `createQuiz(quiz)`.
 */

exports.createQuiz = createQuiz;

/**
* Constants
**/

var State = {
    IDLE : '\'Idle state\'',
    WAITING_FOR_USERS : '\'waiting for other users state\'',
    QUIZ_IN_PROGRESS : '\'quiz in progress state\'',
    QUIZ_ENDED : '\'quiz has ended state\'',
    POST_QUIZ_IDLE : '\'post quiz state\''
}

var Room = {
	WAITING : 'waiting-room',
	QUIZ : 'quiz',
	POST_QUIZ : 'post-quiz'
}

var ERROR = -1;

var CHART_OPTIONS = {
  //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
  scaleBeginAtZero : true,

  //Boolean - Whether grid lines are shown across the chart
  scaleShowGridLines : true,

  //String - Colour of the grid lines
  scaleGridLineColor : "rgba(200,200,200,0.8)",

  //Number - Width of the grid lines
  scaleGridLineWidth : 3,

  //Boolean - Whether to show horizontal lines (except X axis)
  scaleShowHorizontalLines: true,

  //Boolean - Whether to show vertical lines (except Y axis)
  scaleShowVerticalLines: true,

  //Boolean - If there is a stroke on each bar
  barShowStroke : false,

  //Number - Pixel width of the bar stroke
  barStrokeWidth : 2,

  //Number - Spacing between each of the X value sets
  barValueSpacing : 5,

  //Number - Spacing between data sets within X values
  barDatasetSpacing : 0,

  //String - A legend template
  legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
 };


function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

var SERVER_TIMOUT_PERIOD = 10000;

/**
 * Expose `createQuiz(quiz)`.
 */

exports.createQuiz = createQuiz;

/**
 * Creates a new new quiz on a new server.
 * expects 
 *	options: {
 *		quiz:[
 *			{
 *				question, 
 *				choices, 
 *				answer, 
 *				[[timeout]]
 *			}
 *		], 
 *		[[roomsize]], 
 *		[[globalTimeout]]
 *	}
 *
 * @return {server, close}
 * @api public
 */

function createQuiz(options) {
	
	var server = Server.createServer();
	var ee = new EventEmitter();
	var quizTimer;

	/**
	* Quiz validation
	**/

	options = validateOptions(options);

	function validateOptions(options) {
		if(!options)
			error('Missing options')

		if(!Validate.isObject(options))
			error('Options is not an object')

		options.quiz = validateQuiz(options.quiz);
		options.roomsize = validateRoomsize(options.roomsize);
		options.globalTimeout = validateGlobalTimeout(options.globalTimeout);
		return options;		
	}

	function validateQuiz(quiz) {
		if(!quiz)
			error('Missing quiz')

		if(!Validate.isArray(quiz))
			quiz = [quiz]

		quiz = quiz.filter(function(_quiz) {
			return !Validate.isEmpty(_quiz)
		})

		quiz = quiz.map(function(_quiz) {
			var choices = validateChoices(_quiz.choices);
			var answer = validateAnswer(_quiz.answer);

			if(Validate.contains(choices, answer))
				choices = choices.splice(choices.indexOf(answer),1);

			return {
				question: validateQuestion(_quiz.question),
				choices: choices,
				answer: answer,
				timeout: validateTimeout(_quiz.timeout),
				id: quiz.indexOf(_quiz)+1,
				selections: new Array()
			}
		})

		if(Validate.isEmpty(quiz))
			error('No quizes found')

		return quiz;
	}

	function validateRoomsize(roomsize) {
		if(!roomsize) 
			return 1;

		if(Validate.isArray(roomsize))
			roomsize = roomsize.map(Number).filter(Validate.isNumber)[0]

		if(Validate.isEmpty(roomsize))
			return 1;

		if(Validate.isString(roomsize))
			roomsize = Number(roomsize);

		return Math.max(1,roomsize);
	}

	function validateGlobalTimeout(globalTimeout) {
		if(!globalTimeout)
			return;

		if(Validate.isArray(globalTimeout))
			globalTimeout = globalTimeout.map(Number).filter(Validate.isNumber)[0]

		if(Validate.isEmpty(globalTimeout))
			return;

		if(Validate.isString(globalTimeout))
			globalTimeout = Number(globalTimeout);

		return Math.max(3,globalTimeout);
	}

	function validateQuestion(question) {
		if(!question)
			error('Missing question');

		if(Validate.isArray(question))
			question = question[0];

		if(Validate.isEmpty(question))
			error('Question is empty');

		return String(question);
	}

	function validateChoices(choices) {
		if(!choices)
			error('Missing choices')

		if(!Validate.isArray(choices))
			choices = [choices];

		choices = choices.filter(function(choice) {
			return (Validate.isString(choice) || 
					Validate.isNumber(choice)) &&
					!Validate.isEmpty(choice)
		})

		choices = choices.map(String)

		choices = choices.filter(function(choice, pos, array) {
  			return array.indexOf(choice) == pos;
		})

		if(Validate.isEmpty(choices))
			error('No valid choices found')

		return choices;
	}

	function validateAnswer(answer) {
		if(!answer)
			error('Missing answer')

		if(Validate.isArray(answer))
			answer = answer[0];

		if(!Validate.isNumber(answer) && !Validate.isString(answer))
			error('Answer is not a number or string');

		if(Validate.isEmpty(answer))
			error('Answer can not be empty')

		return answer = String(answer)
	}

	function validateTimeout(timeout) {
		if(!timeout || Validate.isEmpty(timeout))
			timeout = validateGlobalTimeout(options.globalTimeout);
		
		if(Validate.isArray(timeout))
			timeout = timeout.map(Number).filter(Validate.isNumber)[0]

		if(Validate.isString(timeout))
			timeout = Number(timeout);

		return Math.max(3,timeout);
	}

	/**
	* Event Emitter
	**/
	ee.on("User entered waiting room", function (socket) {
	    debug("Event occured : User entered waiting room");
	    
	    debug('Stop the timer');
	    timer.stop();

		debug('User joining the room');
	    socket.join(Room.QUIZ);

	    log('There are ' + numberOfUsersIn(Room.QUIZ) + ' users in the room');

	    socket.emit('waiting for users', options.roomsize ? options.roomsize - numberOfUsersIn(Room.QUIZ): undefined)

log('roomsize: '+options.roomsize)
	    if (numberOfUsersIn(Room.QUIZ) == options.roomsize) {
	    	ee.emit('Waiting room size reached');
	    } else {
	    	changeState(State.WAITING_FOR_USERS);
	    }


	});

	ee.on("No users in the waiting room", function () {
	    debug("Event occured : No users in the waiting room");

	    debug('Restart the timer');
	    timer.start();

	    changeState(State.IDLE);
	});

	ee.on("Waiting room size reached", function () {
	    debug("Event occured : Quiz room size reached");

	    changeState(State.QUIZ_IN_PROGRESS);
	});
	 
	ee.on("Quiz manually started", function () {
	    log("Event occured : Quiz manually started");

	    changeState(State.QUIZ_IN_PROGRESS);
	});

	ee.on("Question timed out", function () {
	    log("Event occured : Question timed out");

	    changeState(State.QUIZ_IN_PROGRESS);
	});

	ee.on("All users answered question", function () {
	    log("Event occured : All users answered question");

	    changeState(State.QUIZ_IN_PROGRESS);
	});
	
	ee.on("No more questions", function () {
	    log("Event occured : No more questions");

	    generateResult()

	    changeState(State.QUIZ_ENDED);
	});

	ee.on("No users in the post-quiz room", function () {
	    log("Event occured : No users in the post-quiz room");

	    debug('start server timer')
    	timer.start()

	    changeState(State.POST_QUIZ_IDLE);
	});	

	ee.on("User entered the post-quiz room", function (socket) {
	    log("Event occured : User entered the post-quiz room");

	    debug('Server timer stopped');
	    timer.stop()

	    debug('User joining quiz room');
	    socket.join(Room.QUIZ);

	    changeState(State.POST_QUIZ);
	});	

	var averageRTT = 0;
	var pings = new Array();
	Array.observe(pings, function(changes) {
		if(changes.length) {
	  		averageRTT = pings.reduce(function(a,b) {return a + b}) / pings.length
			debug("average RTT: " + averageRTT)
		}
	});

	// Initialise the server to the IDLE state
	var state = State.IDLE;

	// Define server timeout function
	var timer = new TimerJob(
		{
			interval: SERVER_TIMOUT_PERIOD,
			autoStart: true
		}, 
		function(done) {
			debug('Stop the timer');
			timer.stop();
		    log('Server timed out. Now closing');
			close();
		    done();
		}
	);

/*
	switch (state) {
	    case State.IDLE :
	    	break;
	    case State.WAITING_FOR_USERS :
	    	break;
	    case State.QUIZ_IN_PROGRESS :
	    	break;
	    case State.QUIZ_ENDED :
	    	break;
	    case State.POST_QUIZ_IDLE :
	    	break;
	    default :
	    	break;
	}
*/
	/**
	 * Socket connection that handels the quiz participants
	 **/
	server.io.on('connection', function (socket) {
		switch (state) {
		    case State.IDLE :
		    case State.WAITING_FOR_USERS :
		    	ee.emit('User entered waiting room', socket);
		    	break;
		    case State.QUIZ_IN_PROGRESS :
		    	break;
		    case State.QUIZ_ENDED :
		    	break;
		    case State.POST_QUIZ_IDLE :
		    	ee.emit('User entered the post-quiz room', socket)
		    	break;
		    default :
		    	break;
		}

		socket.on('disconnect', function() {
			switch (state) {
			    case State.QUIZ_IN_PROGRESS :
			    	options.roomsize = numberOfUsersIn(Room.QUIZ)-1;
			    	break;
			    case State.QUIZ_ENDED :
			    	changeState(State.QUIZ_ENDED)
			    case State.IDLE :
			    case State.WAITING_FOR_USERS :
			    case State.POST_QUIZ_IDLE :
			    default :
			    	break;
			}

			log('User has left the room');
			socket.leave(Room.QUIZ);
		});

		var milliseconds = new Date().getTime();
		socket.on('ping', function() {
			log('User sent a ping');
			pings.push(new Date().getTime() - milliseconds);
		});		
		socket.emit('ping'); 

		socket.on('submit answer', function(response) {
			switch (state) {
			    case State.QUIZ_IN_PROGRESS :
		    		log('User submited \''+response.selection+'\'')
		    		
		    		var quiz = options.quiz.find(function(quiz) {
		    			return quiz.id == response.quizId;
		    		})

		    		if(!quiz) {
		    			log('Couldnt not find quiz with id: '+response.quizId);
		    			break;
		    		}
		    				
	    			quiz.selections.push(response.selection);
		    		
		    		if(quiz.selections.length >= options.roomsize){
		    			ee.emit('All users answered question');
		    			return;
		    		}			    		
			    	break;
			    case State.IDLE :
			    case State.WAITING_FOR_USERS :
			    case State.QUIZ_ENDED :
			    case State.POST_QUIZ_IDLE :
			    default :
			    	log('Can not submit answers while quiz is not in progress');
			    	break;
			}
		});


	});

	function log(message) {
		console.log(server.port + ': ' + message);
	}

	function debug(message) {
		console.log(server.port + ': [DEBUG] '+message);
	}	

	function error(error) {
		close();
		throw new Error(error);
	}

	function close() {
		return server.close();
	}

	function numberOfUsersIn(_room) {
	    if(!_room)
	    	return ERROR;

	    var room = server.io.sockets.adapter.rooms[_room]; 
	    
	    if (!room)
	    	return ERROR;

	    return Object.keys(room).length;
	}

	function startQuiz(){
		switch (state) {
		    case State.WAITING_FOR_USERS :
		    	ee.emit('Quiz manually started')
		    	break;
		    case State.IDLE :
		    case State.QUIZ_IN_PROGRESS :
		    case State.QUIZ_ENDED :
		    case State.POST_QUIZ_IDLE :
		    default :
		    	error('Can not start quiz in the current state');
		    	break;
		}
	}

	function makeIterator(array){
	    var nextIndex = 0;
	    
	    return {
	       next: function(){
	           return nextIndex < array.length ?
	               {value: array[nextIndex++], done: false} :
	               {done: true};
	       },
	       current: function(){
	           return nextIndex < array.length ?
	               {value: array[nextIndex], done: false} :
	               {done: true};
	       }
	    };
	}

	questionIt = makeIterator(options.quiz);

	function nextQuestion() {
		var question = questionIt.next();

		if(question.done)
			return;

		question = question.value;

		clearTimeout(quizTimer);
		var timeout = question.timeout ? question.timeout : (options.globalTimeout ? options.globalTimeout : undefined)  
		if (timeout){
			debug('Start quiz timer');
			quizTimer = setTimeout(function() {
				ee.emit('Question timed out');
			}, timeout*1000+2*averageRTT);
		}

		log('Question has been sent')
		server.io.in(Room.QUIZ).emit('quiz', {
			question: question.question,
			choices: shuffle(question.choices.concat([question.answer])),
			timeout: question.timeout,
			id: question.id
		})
		
		return question;
	}

	function generateResult() {
	    
	    var data = {
	    	labels: options.quiz.map(function(_quiz) {return _quiz.question}),
	    	datasets: [
	    		{
	    			label: "Quiz",
					fillColor: "rgba(51,204,51,1)",
					strokeColor: "rgba(51,204,51,1)",
					data:  options.quiz.map(function(_quiz) {
						return !Validate.isEmpty(_quiz.selections) ? 
							_quiz.selections.filter(function (selection) {
								return _quiz.answer == selection
							}).length :
							0
					})
				}
			]
		};

	    server.io.in(Room.QUIZ).emit('finish', {
	    	message: 'Thank you for participating',
	    	data: data,
	    	options: CHART_OPTIONS
	    });
	}

	function changeState(newState) {

		function accept(newState) {
	    	log('Transition accepted ' + state + ' to ' + newState);
	    	state = newState;
		}

		function reject(newState) {
	    	log('Invalid transition ' + state + ' to ' + newState);			
		}

		switch (state) {
		    case State.IDLE :
	    		switch (newState) {
				    case State.IDLE :
				    case State.WAITING_FOR_USERS :
				    	accept(newState);
				    	break;
				    default :
				    	reject(newState);
				    	break;
				}

		    	break;
		    case State.WAITING_FOR_USERS :
				switch (newState) {
				    case State.IDLE :
				    case State.WAITING_FOR_USERS :
				    	if(numberOfUsersIn(Room.QUIZ) == options.roomsize) {
				    		ee.emit('Waiting room size reached');
				    		return;
				    	}
				    case State.QUIZ_IN_PROGRESS :
				    	if(!nextQuestion()){
				    		ee.emit('No more questions');
				    		return;
				    	}
				    	accept(newState);
				    	break;
				    default :
				    	reject(newState);
				    	break;
				}
		    	break;
		    case State.QUIZ_IN_PROGRESS :
		    	switch (newState) {
				    case State.QUIZ_IN_PROGRESS :
				    	if(!nextQuestion()){
				    		ee.emit('No more questions');
				    		return;
				    	}
				    case State.QUIZ_ENDED :
				    	accept(newState);
				    	break;
				    default :
				    	reject(newState);
				    	break;
				}
		    	break;
		    case State.QUIZ_ENDED :
		    	switch (newState) {
				    case State.QUIZ_ENDED :
				    	if(numberOfUsersIn(Room.QUIZ) <= 0){
				    		ee.emit("No users in the post-quiz room");
				    		return;
				    	}
				    case State.POST_QUIZ_IDLE :
				    	accept(newState);
				    	break;
				    default :
				    	reject(newState);
				    	break;
				}
		    	break;
		    case State.POST_QUIZ_IDLE :
		    	switch (newState) {
				    case State.POST_QUIZ_IDLE :
				    case State.QUIZ_ENDED :
				    	accept(newState);
				    	break;
				    default :
				    	reject(newState);
				    	break;
				}
		    	break;
		    default :
		    	break;
		}
		debug('Current state: ' + state);
	}

	return {
		server: server,
		close: close,
		startQuiz: startQuiz,
		getState : function () {
			return state;
		} 
	};
}

