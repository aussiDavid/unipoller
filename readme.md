Introduction
------------
UniPoller

UniPoller is a quick surveying application for short quizzes and polls in the classroom to improve engagement of students.

Download
--------

To download the application you can use the git command:

`git clone https://bitbucket.org/aussiDavid/unipoller.git`

or downloading the compressed zip [here](https://bitbucket.org/aussiDavid/unipoller/get/master.zip). Extract the zip file.


Installation and Requirements
-----------------------------

This application requires [node.js](https://nodejs.org/) and [git](https://git-scm.com/downloads) to be installed.

Enter the application root directory. To install the application and modules use the command:

`npm install`

Running the Application
-----------------------

The application runs on [http://localhost port 3000(http://localhost:3000)](http://localhost:3000). Once the application is running, enter this address in the URL to view the application.

To start the application use the command:

`npm start`

To stop the application press Ctrl/Cmd-C.