var assert = require("assert");
var expect = require("expect");
const SERVER_LIB = "../lib/server"
var Browser = require('zombie');
var Server = require(SERVER_LIB);
var Quiz = require('../src/quiz');
var stringify = require('json-stable-stringify');

const SERVER_ADDRESS = "http://localhost";
const BUILD_QUIZ = "build_quiz.html";

var browser = new Browser();

describe("Running", function() {
    describe("the server", function() {

        Server.closeAll();

        it("should exists when included with no errors", function() {
            expect(Server).toExist();
        });

        it("should start on port 3000", function() {
            var server = Server.createServer(3000);
            expect(server.port).toBe(3000);
            expect(server.server).toExist();
            expect(server.io).toExist();
            server.close();
        });

        it("should allow multiple servers", function() {
            var server = Server.createServer();
            expect(server.port).toBeGreaterThan(100);
            expect(server.server).toExist();
            expect(server.io).toExist();

           var server2 = Server.createServer();
           expect(server2.port).toNotBe(server.port);
           expect(server2.server).toExist();
           expect(server2.io).toExist(); 

           server.close();
           server2.close();
        });

    });

    describe("the quiz", function() {

        it("should exists when included with no errors", function() {
            expect(Quiz).toExist();
        });

        it("should start normally", function() {
            var quiz = Quiz.createQuiz({quiz:[{question:"This is a test?", choices:['2','4'], answer:'5'}]});
            expect(quiz).toExist();
            expect(quiz.server).toExist();
            expect(quiz.getState()).toBe('\'Idle state\'');
            quiz.close();
        });

        it("should throw error with no options", function() {
            expect(function () {
                Quiz.createQuiz();
            }).toThrow('/Missing options/');
        });

        [
            // Test empty argument
            {arg:{}, error:/Missing quiz/},
            // Test empty quiz argument
            {arg:{quiz:[{}]}, error:/No quizes found/},
            {arg:{quiz:[{question:"This is a test?"}]}, error:/Missing choices/},
            {arg:{quiz:[{question:"This is a test?", choices:['2','4']}]}, error:/Missing answer/},            
            {arg:{quiz:[{question:"This is a test?", choices:['2','4'], answer:'5'}]}},
            // Test choices argument
            {arg:{quiz:[{question:"This is a test?", choices:['2',''], answer:'1'}], roomsize:2}},
            {arg:{quiz:[{question:"This is a test?", choices:['2',''], answer:'2'}], roomsize:2}},
            {arg:{quiz:[{question:"This is a test?", choices:[''], answer:'1'}], roomsize:2}, error:/No valid choices found/},
            {arg:{quiz:[{question:"This is a test?", choices:[], answer:'1'}], roomsize:2}, error:/No valid choices found/},
            {arg:{quiz:[{question:"This is a test?", choices:{}, answer:'1'}], roomsize:2}, error:/No valid choices found/},
            {arg:{quiz:[{question:"This is a test?", choices:{choice:'2'}, answer:'1'}], roomsize:2}, error:/No valid choices found/},
            {arg:{quiz:[{question:"This is a test?", choices: 2, answer:'1'}], roomsize:2}},
            {arg:{quiz:[{question:"This is a test?", choices:'2', answer:'1'}], roomsize:2}},
            {arg:{quiz:[{question:"This is a test?"             , answer:'1'}], roomsize:2}, error:/Missing choices/},
            // Test answers argument
            {arg:{quiz:[{question:"This is a test?", choices:['2','4'], answer:''}], roomsize:2}, error:/Missing answer/},
            {arg:{quiz:[{question:"This is a test?", choices:['2','4'], answer:[]}], roomsize:2}, error:/Answer is not a number or string/},
            {arg:{quiz:[{question:"This is a test?", choices:['2','4']           }], roomsize:2}, error:/Missing answer/},
            {arg:{quiz:[{question:"This is a test?", choices:['2','4'], answer:1}], roomsize:2}},
            {arg:{quiz:[{question:"This is a test?", choices:['2','4'], answer:'1'}], roomsize:2}},
            // Test question argument
            {arg:{quiz:[{question:"", choices:['2','4'], answer:'1'}], roomsize:2}, error:/Missing question/},
            {arg:{quiz:[{question:"Test?", choices:['2','4'], answer:'1'}], roomsize:2}},
            {arg:{quiz:[{question:"Test", choices:['2','4'], answer:'1'}], roomsize:2}},
            {arg:{quiz:[{                 choices:['2','4'], answer:'1'}], roomsize:2}, error:/Missing question/},
            {arg:{quiz:[{question:[], choices:['2','4'], answer:'1'}], roomsize:2}, error:/Question is empty/},
            {arg:{quiz:[{question:1, choices:['2','4'], answer:'1'}], roomsize:2}},
            // Test roomsize argument
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}],            }},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:''}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:[]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:{}}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:-1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:'5'}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:'a'}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:['a',2]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:[1,"2"]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:{op:1}}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:[{op:1}]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:[{op:1},3]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:1000}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:1000000}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:function() {return 300}}},
            // Test global timeout argument
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:0}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:-1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:'1'}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:'a'}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:[]}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:{}}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'}], roomsize:3, globalTimeout:[1, '2']}},
            // Test timeout argument
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1'            }], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:1}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:-1}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:'2'}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:'a'}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:[]}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:{}}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:[1,'2']}], roomsize:3, globalTimeout:1}},
            {arg:{quiz:[{question:'Test?', choices:['2','4'], answer:'1', timeout:['a','2']}], roomsize:3, globalTimeout:1}}
        ].forEach(function(option) {
            if(option.error) {
                it("should throw error \'"+option.error+"\' for option: " + stringify(option.arg), function() {
                    expect(function () {
                        Quiz.createQuiz(option.arg).close();
                    }).toThrow(option.error);
                });
            } else {
                it("should not throw error for option: " + stringify(option.arg), function() {
                    expect(function () {
                        Quiz.createQuiz(option.arg).close();
                    }).toNotThrow();
                });
            }
        });
    });
});

// Test