/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var socketIo = require('socket.io');

var servers = new Array();

/**
 * Expose `createServer()`.
 */

exports.createServer = createServer;

/**
 * Create a new server to handel a new quiz.
 *
 * @return {Server object}
 * @api public
 */

function createServer(port) {
	var app = express();
	app.use(express.static(__dirname + '/../src/public'));
	var server = http.createServer(app);
	servers.push(server);
	var io = socketIo.listen(server);

	server.listen(port ? port : 0,function() {
		console.log('Server listening at port %d', this.address().port);
	});

	server.on('error', function (e) {
		console.log(e);
	  	server.close();
		servers.splice(0,1,server);  	
	});

	return {
		server: server,
		port: server.address().port,
		io: io,
		close: function() {
			console.log("Closing server : " +  server.address().port);
			server.close();
			servers.splice(0,1,server);
		}
	};
}

/**
 * Expose `closeAll()`.
 */

exports.closeAll = closeAll;

/**
 * Closes all the servers provided by the module.
 *
 * @api public
 */

function closeAll() {
	servers.forEach(function(server) {
		server.close()
	});
	servers = new Array();
}